import os


DEBUG = False
SQLALCHEMY_TRACK_MODIFICATIONS = False

KEY_PATH = os.environ.get('KEY_PATH', None)
CERT_PATH = os.environ.get('CERT_PATH', None)
VAL_PERIOD = os.environ.get('CERT_VALIDITY_PERIOD', 30)

OIDC_CLIENT_SECRETS = os.environ.get('OIDC_CONFIG_FILE', None)
OIDC_RESOURCE_SERVER_ONLY = True
OIDC_INTROSPECTION_AUTH_METHOD = 'client_secret_post'

COUNTRY_CODES_PATH = os.environ.get('COUNTRY_CODES_PATH', None)

ACCESS_ROLE = os.environ.get('ACCESS_ROLE', '')
