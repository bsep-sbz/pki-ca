from pki.ca.config.base import *

DB_ADDRESS = os.environ['DB_ADDRESS']
DB_PORT = os.environ['DB_PORT']
DB_NAME = os.environ['DB_NAME']
DB_USER = os.environ['DB_USER']
DB_PASSWORD = os.environ['DB_PASSWORD']

SQLALCHEMY_DATABASE_URI = f'mysql+mysqlconnector://{DB_USER}:{DB_PASSWORD}@{DB_ADDRESS}:{DB_PORT}/{DB_NAME}'

COUNTRY_CODES_PATH = '/app/pki/ca/data/country-codes.csv'
OIDC_CLIENT_SECRETS = '/app/pki/ca/config/keycloak.json'

OIDC_ISSUER = 'https://auth.megatravel.com/auth/realms/pki'
OIDC_CLIENT_ID = 'pki-ca-eu'
OIDC_CLIENT_SECRET = 'd50287bb-4bda-4ea5-be1e-8bb8da5d6f74'
