from pki.ca.config.base import *


DEBUG = True

KEY_PATH = '/home/alex/projects/megatravel/pki-ca/secrets/ca.key'
CERT_PATH = '/home/alex/projects/megatravel/pki-ca/secrets/ca.cert'
VAL_PERIOD = 730

OIDC_CLIENT_SECRETS = '/home/alex/projects/megatravel/pki-ca/pki/ca/config/keycloak.json'

SQLALCHEMY_DATABASE_URI = 'sqlite:////tmp/ca.db'
COUNTRY_CODES_PATH = '/home/alex/projects/megatravel/pki-ca/pki/ca/data/country-codes.csv'

ACCESS_ROLE = 'infosec'
