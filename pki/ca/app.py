import os
from io import BytesIO

import pem
from cryptography import x509
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.serialization import load_pem_private_key
from flask import Flask

from pki.ca.extensions import *
# from pki.ca.routes import router
from pki.ca.resources import router


def create_app():
    app = Flask(__name__)
    configure(app)
    register_extensions(app)
    register_blueprints(app)
    return app


def configure(app):
    mode = os.environ['MODE']
    app.config.from_pyfile(os.path.join('config', mode + '.py'))

    chain = pem.parse_file(app.config['CERT_PATH'])
    app.config['CERT_CHAIN'] = [bytes(str(c), encoding='utf-8') for c in chain]

    app.config['CERT_PEM'] = str(chain[0])
    cert = BytesIO(bytes(str(chain[0]), encoding='utf-8'))
    app.config['CERT'] = x509.load_pem_x509_certificate(
        data=cert.read(),
        backend=default_backend()
    )

    with open(app.config['KEY_PATH'], 'rb') as f:
        app.config['KEY'] = load_pem_private_key(
            data=f.read(),
            password=None,
            backend=default_backend()
        )

    with open(app.config['COUNTRY_CODES_PATH'], 'r') as f:
        codes = [line.split(',')[-1].strip() for line in f.readlines()[1:]]
        app.config['COUNTRY_CODES'] = codes


def register_extensions(app):
    db.init_app(app)
    migrate.init_app(app, db)
    oidc.init_app(app)


def register_blueprints(app):
    app.register_blueprint(router)


application = create_app()
