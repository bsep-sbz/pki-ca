from functools import wraps

from flask import (
    abort,
    current_app as app,
    g,
    jsonify,
    request,
    Blueprint
)
from marshmallow import ValidationError
from sqlalchemy.orm.exc import NoResultFound

# from pki.ca.extensions import oidc
# from pki.ca.routes import router
from pki.ca.services import (
    issue_certificate,
    get_certificate,
    list_certificates,
    revoke_certificate,
)
from pki.ca.serialization import IssueCertificateRequest


router = Blueprint('router', __name__)


def protected(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        for key, value in g.oidc_token_info.items():
            print(key, value)
        roles = g.oidc_token_info['realm_access']['roles']
        if app.config['ACCESS_ROLE'] not in roles:
            abort(403)
        return func(*args, **kwargs)
    return wrapper


@router.route('/health')
def health_check():
    return {'status': 'OK'}


@router.route('/certificates/issue', methods=['POST'])
# @oidc.accept_token(True, ['profile', 'email'])
# @protected
def issue_cert():
    json_data = request.get_json()
    cert_request = IssueCertificateRequest()
    if not json_data:
        return jsonify({'message': 'No input data provided'}), 400
    try:
        req = cert_request.load(data=json_data)
    except ValidationError as err:
        return jsonify(err.messages), 422

    return issue_certificate(req.data)


@router.route('/certificates/revoke/<string:pk>', methods=['POST'])
# @oidc.accept_token(True, ['profile', 'email'])
# @protected
def revoke_cert(pk):
    try:
        revoke_certificate(pk)
        return "OK", 200
    except NoResultFound:
        abort(404)


@router.route('/certificates/<string:pk>', methods=['GET'])
def get_cert(pk):
    try:
        return get_certificate(pk)
    except NoResultFound:
        abort(404)


@router.route('/certificates', methods=['GET'])
def list_certs():
    return list_certificates()
