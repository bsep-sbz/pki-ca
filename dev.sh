#!/usr/bin/env bash
source venv/bin/activate
cd ./pki/ca
flask db init
flask db migrate
flask db upgrade
