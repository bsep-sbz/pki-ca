#!/usr/bin/env bash
while ! nc -z db 3306; do sleep 3; done

#cp /x509/root.cert /usr/local/share/ca-certificates/megatravel-root.crt
#chmod 664 /usr/local/share/ca-certificates/megatravel-root.crt
#update-ca-certificates
#
#export REQUESTS_CA_BUNDLE=/etc/ssl/certs/ca-certificates.crt

cat /x509/root.cert >> /usr/local/lib/python3.6/site-packages/certifi/cacert.pem

cd pki/ca

flask db upgrade
gunicorn -b 0.0.0.0:8080 app
