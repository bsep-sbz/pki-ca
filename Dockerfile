FROM python:3.6-slim

RUN apt-get -qq update
RUN apt-get -qq -y install netcat
RUN apt-get -qq -y install iputils-ping

WORKDIR /app

COPY requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt

COPY start.sh .
COPY setup.py .
COPY pki ./pki
RUN pip install .

CMD ["./start.sh"]
